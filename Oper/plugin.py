###
# Copyright (c) 2012, Aha2Y
# All rights reserved.
#
#
###

import supybot.utils as utils
from supybot.commands import *
import supybot.plugins as plugins
import supybot.ircutils as ircutils
import supybot.callbacks as callbacks
import supybot.ircmsgs as ircmsgs
from supybot.i18n import PluginInternationalization, internationalizeDocstring

_ = PluginInternationalization('Oper')

@internationalizeDocstring
class Oper(callbacks.Plugin):
    """Add the help for "@plugin help Oper" here
    This should describe *how* to use this plugin."""
    threaded = True

    def do005(self, irc, msg): 
        irc.sendMsg(ircmsgs.IrcMsg(command="OPER", args=[self.registryValue("username"), self.registryValue("password")]))
        irc.sendMsg(ircmsgs.IrcMsg(command="JOIN", args=[self.registryValue("channel")]))
    
Class = Oper


# vim:set shiftwidth=4 softtabstop=4 expandtab textwidth=79:
