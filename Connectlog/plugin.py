###
# Copyright (c) 2012, Aha2Y
# All rights reserved.
#
#
###

import supybot.utils as utils
from supybot.commands import *
import supybot.ircdb as ircdb
import supybot.plugins as plugins
import supybot.ircutils as ircutils
import supybot.callbacks as callbacks
import supybot.ircmsgs as ircmsgs
from supybot.i18n import PluginInternationalization, internationalizeDocstring

_ = PluginInternationalization('Connectlog')

@internationalizeDocstring
class Connectlog(callbacks.Plugin):
    """Add the help for "@plugin help Connectlog" here
    This should describe *how* to use this plugin."""
    threaded = True

    global logchan
    global debugchan

    def doNotice(self, irc, msg):
        if irc.network == "IotaIRC":
            logchan = self.registryValue("channel")
            #print msg.args[1]
            snotice = msg.args[1].split()
            
            if "*** Notice -- Client connecting" in msg.args[1]:
                if snotice[5] == "at":
                    msg = "14Client:7 {0} 14connected to:7 {1} 14with hostname:7 {2}"
                    irc.sendMsg(ircmsgs.privmsg(logchan, "%s" % msg.format(snotice[7], snotice[6].strip(":"), snotice[8].strip("()"))))
                    irc.sendMsg(ircmsgs.IrcMsg(command="locops", args=[msg.format(snotice[7], snotice[6].strip(":"), snotice[8].strip("()"))]))
                    
                elif snotice[5] == "on":
                    msg = "14Client:7 {0} 14connected to:7 mudkip.iotairc.net 14with hostname:7 {1}"
                    irc.sendMsg(ircmsgs.privmsg(logchan, "%s" % msg.format(snotice[8], snotice[9].strip("()"))))
                    irc.sendMsg(ircmsgs.IrcMsg(command="locops", args=[msg.format(snotice[8], snotice[9].strip("()"))]))
                
            elif "*** Notice -- Client exiting" in msg.args[1]:              
                msg = "14Client:7 {0} 14disconnected:7 {1}"  
                if snotice[4] == "exiting":
                    user = snotice[7].split("!", 1)
                    irc.sendMsg(ircmsgs.privmsg(logchan, "%s" % msg.format(user[0], " ".join(snotice[8:])[1:-1])))
                    irc.sendMsg(ircmsgs.IrcMsg(command="locops", args=[msg.format(user[0], " ".join(snotice[8:])[1:-1])]))

                if snotice[4] == "exiting:":
                    irc.sendMsg(ircmsgs.privmsg(logchan, "%s" % msg.format(snotice[5], " ".join(snotice[7:])[1:-1])))
                    irc.sendMsg(ircmsgs.IrcMsg(command="locops", args=[msg.format(snotice[5], " ".join(snotice[7:])[1:-1])]))

Class = Connectlog


# vim:set shiftwidth=4 softtabstop=4 expandtab textwidth=79:
